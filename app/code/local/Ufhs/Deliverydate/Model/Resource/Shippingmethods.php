<?php
class Ufhs_Deliverydate_Model_Resource_Shippingmethods extends Mage_Core_Model_Resource_Db_Abstract
{
	protected $_isPkAutoIncrement = false;

	public function _construct()
	{
		$this->_init('deliverydate/shippingmethods', 'id');
	}
}