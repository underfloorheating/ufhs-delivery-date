<?php

class Ufhs_Deliverydate_Model_Observer
{
	public function writeData($event)
	{
		$date = Mage::app()->getRequest()->getParam('delivery-date');
		Mage::getSingleton('core/session')->unsetData('deliveryData');
		Mage::getSingleton('core/session')->setData('deliveryData', $date);
	}

	public function readData($event)
	{
		$date = Mage::getSingleton('core/session')->getData('deliveryData');
		Mage::getSingleton('core/session')->unsetData('deliveryData');
		$orderId = $event->getData()['order_ids'][0];
		$incrementId = Mage::getModel('sales/order')->load($orderId)->getData()['increment_id'];

		if (strtotime($date) > 0) {
			Mage::getModel('deliverydate/orders')
			->setOrderId($incrementId)
			->setDate($date)
			->save();
		}
	}
}