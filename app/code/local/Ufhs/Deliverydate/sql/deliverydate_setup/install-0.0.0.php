<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/daysofweek')};
	CREATE TABLE {$installer->getTable('deliverydate/daysofweek')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`label` varchar(9) NOT NULL,
		`allowed` tinyint(1) NOT NULL,
		`optional` tinyint(1) NOT NULL,
		`reason` varchar(512) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	INSERT INTO {$installer->getTable('deliverydate/daysofweek')} (id,label,allowed,optional,reason) VALUES
	(0,'Sunday',0,0,''),
	(1,'Monday',1,0,''),
	(2,'Tuesday',1,0,''),
	(3,'Wednesday',1,0,''),
	(4,'Thursday',1,0,''),
	(5,'Friday',1,0,''),
	(6,'Saturday',1,1,'Saturday delivery can be choosen at checkout.');

	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/disallowed')};
	CREATE TABLE {$installer->getTable('deliverydate/disallowed')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`date` date NOT NULL,
		`reason` varchar(512) NOT NULL,
		`recurring` tinyint(1) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/colours')};
	CREATE TABLE {$installer->getTable('deliverydate/colours')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`label` varchar(12) NOT NULL,
		`colour` varchar(7) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	INSERT INTO {$installer->getTable('deliverydate/colours')} (id,label,colour) VALUES
	(1,'Disallowed','#E60004'),
	(2,'Optional','#00E6004'),
	(3,'Selected','#0400E6');

	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/orders')};
	CREATE TABLE {$installer->getTable('deliverydate/orders')} (
		`order_id` int(11) unsigned NOT NULL,
		`date` date NOT NULL,
		PRIMARY KEY (`order_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/config')};
	CREATE TABLE {$installer->getTable('deliverydate/config')} (
		`id` varchar(255) NOT NULL,
		`value` varchar(255) NOT NULL,
		`frontname` varchar(255) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	INSERT INTO {$installer->getTable('deliverydate/config')} (`id`, `value`, `frontname`) VALUES
	('cutoff', '15', 'Delivery Cutoff Time'),
	('shipping-attribute', 'package_id', 'Shipping Method Attribute');

	DROP TABLE IF EXISTS {$installer->getTable('deliverydate/shippingmethods')};
	CREATE TABLE {$installer->getTable('deliverydate/shippingmethods')} (
		`id` varchar(255) NOT NULL,
		`days` int(255) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
$installer->endSetup();