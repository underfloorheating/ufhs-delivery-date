<?php
class Ufhs_Deliverydate_Block_Adminhtml_Orders_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('deliverydateOrdersGrid');
		$this->setDefaultSort('date');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('deliverydate/orders')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('deliverydate')->__('Order ID'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'order_id'
		));
		$this->addColumn('date', array(
			'header' => Mage::helper('deliverydate')->__('Date'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'date'
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("deliverydate_block_adminhtml_orders_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/sales_order/view', array('order_id' => Mage::getModel('sales/order')->loadByIncrementId($row->getData('order_id'))->getId()));
	}

	public function getEmptyText()
	{
		return $this->__('No orders have been submitted yet.');
	}
}