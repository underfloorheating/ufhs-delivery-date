<?php
class Ufhs_Deliverydate_Block_Adminhtml_Newdisallowed_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('New Disallowed Date');
	}

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'newdisallowed',
			'action' => $this->getUrl('*/*/adddisallowed'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('date_form', array(
			'legend' => Mage::helper('deliverydate')->__('Date Data')
		));

		$fieldset->addField('date', 'date', array(
			'label' => Mage::helper('deliverydate')->__('Date'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'date',
			'format' => 'yyyy-mm-dd',
			'style' => 'width:150px;'
		))->setType('date');

		$fieldset->addField('reason', 'text', array(
			'label' => Mage::helper('deliverydate')->__('Reason'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'reason'
		));

		$fieldset->addField('recurring', 'checkbox', array(
			'label' => Mage::helper('deliverydate')->__('Recurring'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'recurring'
		));

		return parent::_prepareForm();
	}
}