<?php
class Ufhs_Deliverydate_Block_Adminhtml_Disalloweddates extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_disalloweddates';
		$this->_blockGroup = 'deliverydate';
		$this->_headerText = Mage::helper('deliverydate')->__('Disallowed Dates');
		parent::__construct();
		$this->_removeButton('add');
	}
}