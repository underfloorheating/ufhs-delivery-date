<?php
class Ufhs_Deliverydate_Block_Adminhtml_Neworder_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('New Order');
	}

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'neworder',
			'action' => $this->getUrl('*/*/addorder'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('date_form', array(
			'legend' => Mage::helper('deliverydate')->__('Order Data')
		));

		$fieldset->addField('order_id', 'text', array(
			'label' => Mage::helper('deliverydate')->__('Order ID'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'order_id'
		));

		$fieldset->addField('date', 'date', array(
			'label' => Mage::helper('deliverydate')->__('Date'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'date',
			'format' => 'yyyy-mm-dd',
			'style' => 'width:150px;'
		))->setType('date');

		return parent::_prepareForm();
	}
}