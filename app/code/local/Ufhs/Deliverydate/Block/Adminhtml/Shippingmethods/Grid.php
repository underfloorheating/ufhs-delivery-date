<?php
class Ufhs_Deliverydate_Block_Adminhtml_Shippingmethods_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('deliverydateShippingmethodsGrid');
		$this->setDefaultSort('days');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('deliverydate/shippingmethods')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('deliverydate')->__('ID'),
			'align' => 'left',
			'width' => '500px',
			'index' => 'id'
		));

		$this->addColumn('days', array(
			'header' => Mage::helper('deliverydate')->__('Days'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'days'
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("deliverydate_block_adminhtml_orders_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/editshippingmethod', array('id' => $row->getData('id')));
	}

	public function getEmptyText()
	{
		return $this->__('We have no shipping methods yet, try syncing.');
	}
}